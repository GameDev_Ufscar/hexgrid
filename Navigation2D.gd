extends Navigation2D

onready var sprite : Sprite = $Sprite
onready var line : Line2D = $Line2D
onready var camera : Camera2D = $"/root/Game/Camera2D"

var path : PoolVector2Array
var goal : Vector2

export var speed := 250

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)

func _input(event: InputEvent):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			goal = to_local(get_global_mouse_position())
			path = self.get_simple_path(sprite.position, goal, false)
			line.points = PoolVector2Array(path)
			line.show()
			
func _process(delta: float) -> void:
	if !path:
		line.hide()
		return
	if path.size() > 0:
		var d: float = sprite.position.distance_to(path[0])
		if d > 10:
			sprite.position = sprite.position.linear_interpolate(path[0], (speed * delta)/d)
		else:
			path.remove(0)