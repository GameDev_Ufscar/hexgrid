extends Camera2D

export var MAX_ZOOM = 2
export var MIN_ZOOM = 0.25
export var ZOOM_STEP = 1.1
export var MOVE_SPEED = 1000
export var MOVE_ACCEL = 50
export var SHOULD_DRAG = true
export var SHOULD_ZOOM = true

var current_accel = 1

onready var viewport_rect = get_viewport().get_visible_rect()

func handle_drag(delta):
	
	if !SHOULD_DRAG:
		return
	
	var mouse_pos = get_viewport().get_mouse_position()
	
	var move = Vector2(0, 0)
	
	if mouse_pos.x <= viewport_rect.position.x or Input.is_action_pressed("ui_left"):
		move.x -= 1
	
	if mouse_pos.x >= viewport_rect.end.x - 2 or Input.is_action_pressed("ui_right"):
		move.x += 1
		
	if mouse_pos.y <= viewport_rect.position.y or Input.is_action_pressed("ui_up"):
		move.y -= 1
		
	if mouse_pos.y >= viewport_rect.end.y - 2 or Input.is_action_pressed("ui_down"):
		move.y += 1
		
	if move == Vector2(0, 0):
		current_accel = 1
	
	current_accel = min(current_accel, MOVE_SPEED * 2)
		
	$"../Camera2D".offset += move.normalized() * delta * (MOVE_SPEED + current_accel)
	
	current_accel += MOVE_ACCEL

func handle_zoom(event):
	if !SHOULD_ZOOM:
		return
	
	var zoom = 0
	
	if event.is_action_pressed("ui_zoom_in"):
		zoom = 1/ZOOM_STEP
	elif event.is_action_pressed("ui_zoom_out"):
		zoom = ZOOM_STEP
	else:
		return
		
	var next_zoom = self.scale + Vector2(1, 1) * zoom
	
#	if next_zoom.x < MIN_ZOOM:
#		next_zoom = Vector2(1, 1) * MIN_ZOOM
#
#	if next_zoom.x > MAX_ZOOM:
#		next_zoom = Vector2(1, 1) * MAX_ZOOM
	
#	zoom_at_point(zoom, get_viewport().size/2)
	zoom_at_point(zoom, get_viewport().get_mouse_position())
		
func zoom_at_point(zoom_change, point):
    var c0 = global_position # camera position
    var v0 = get_viewport().size # vieport size
    var c1 # next camera position
    var z0 = zoom # current zoom value
    var z1 = z0 * zoom_change # next zoom value

    c1 = c0 + (-0.5*v0 + point)*(z0 - z1)
    zoom = z1
    global_position = c1
		
func _process(delta):
	handle_drag(delta)
		
func _input(event):
	handle_zoom(event)